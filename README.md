ics-ans-role-firewalld
======================

Ansible role to install firewalld.

Requirements
------------

- ansible >= 2.4
- molecule >= 2.6

Role Variables
--------------

```yaml
firewalld_services:
  - https
firewalld_trusted_cidrs:
  - 192.168.1.0/24
firewalld_ports:
  - 9100/TCP
```

You can pass a list of services to enable in the firewall.

Example Playbook
----------------

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-firewalld
```

If you want to pass services from another role, instead of defining firewalld
as a dependency, you can use `include_role` in your tasks:

```yaml
- name: add http to the firewall
    include_role:
      name: ics-ans-role-firewalld
    vars:
      firewalld_services:
        - http
```

Otherwise, add it as a dependency to be sure it's started and just use the firewalld module:

```yaml
- name: add http to the firewall
  firewalld:
    service: http
    permanent: true
    immediate: true
    state: enabled
```

License
-------

BSD 2-clause
