import os
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')


def test_firewalld_service(host):
    service = host.service('firewalld')
    assert service.is_enabled
    assert service.is_running


def test_ssh_enabled_in_firewall(host):
    cmd = host.run('firewall-cmd --list-all --permanent | grep services')
    services = cmd.stdout
    assert 'ssh' in services
