import os
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('ics-ans-role-firewalld-services')


def test_services_enabled_in_firewall(host):
    cmd = host.run('firewall-cmd --list-all --permanent | grep services')
    services = cmd.stdout
    for item in ('http', 'rpc-bind'):
        assert item in services


def test_cidr_trusted_in_firewall(host):
    cmd = host.run('firewall-cmd --list-all --permanent --zone=trusted | grep sources')
    trusted = cmd.stdout
    for item in ('10.3.42.0/24', '10.10.10.42/32'):
        assert item in trusted


def test_ports_in_firewall(host):
    cmd = host.run('firewall-cmd --list-all --permanent | grep ports')
    trusted = cmd.stdout
    for item in ('9100/tcp'):
        assert item in trusted
